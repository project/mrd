## CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Recommended modules
  - Installation
  - Configuration
  - FAQ
  - Maintainers

## INTRODUCTION

The Media Revision Delete module lets you to track and prune old
revisions of media types.

  - For a full description of the module, visit the project page:
    https://www.drupal.org/project/media\_revision\_delete

  - To submit bug reports and feature suggestions, or to track changes:
    https://www.drupal.org/project/issues/search/media\_revision\_delete

## REQUIREMENTS

No special requirements.

## RECOMMENDED MODULES

  - Drush Help (https://www.drupal.org/project/drush\_help): Improves
    the module help page showing information about the module’s Drush
    commands.

## INSTALLATION

  - Install as you would normally install a contributed Drupal module.
    See:
    https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
    for further information.

## CONFIGURATION

  - Configure the module in Administration » Configuration » Media
    » Media Revision Delete:

      - You can set how many revisions you want to delete per cron run
        and how often should revisions be deleted when cron runs. You
        can save the configuration and optionally start a batch job to
        delete old revisions for tracked media types. For this you
        need the ‘Administer Media Revision Delete’ permission.

  - Configure each media type in Administration » Structure » Media
    types » Media type name:

      - Under the Publishing options tab, mark “Limit the number of
        revisions for this media type” and set the maximum number of
        revisions to keep.

  - Drush commands

      - drush media-revision-delete

        Deletes old media revisions for a given media type.

      - drush mrd-delete-cron-run

        Configures how many revisions to delete per cron run.

      - drush mrd-last-execute

        Get the last time that the media revision delete was made.

      - drush mrd-set-time

        Configures the frequency with which to delete revisions when
        cron runs.

      - drush mrd-get-time

        Shows the frequency with which to delete revisions when cron
        runs.

      - drush mrd-when-to-delete-time

        Configures the time options for the inactivity time that the
        revision must have to be deleted.

      - drush mrd-minimum-age-to-delete-time

        Configures time options for the minimum age that the revision
        must be to be deleted.

      - drush mrd-delete-prior-revisions

        Delete all revisions prior to a revision.

## FAQ

Q: How can I delete prior revisions?

A: When you are deleting a revision of a media, a new checkbox will
appear in a fieldset saying: “Also delete X revisions prior to this
one.”; if you check it, all prior revisions will be deleted as well
for the given media. If you are deleting the oldest revision, the
checkbox will not appear as no prior revisions are available.

## MAINTAINERS

* Andreas Daoutis (daou) - https://www.drupal.org/u/daou

This project has been sponsored by:

  - SMAL (https://www.smal.de)
