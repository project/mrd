<?php

namespace Drupal\media_revision_delete\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\media\MediaTypeInterface;
use Drupal\media_revision_delete\MediaRevisionDeleteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media type revision deletion confirmation form.
 */
class MediaTypeRevisionsDeleteForm extends ConfirmFormBase
{

  /**
   * The media type object.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected MediaTypeInterface $mediaType;

  /**
   * The media revision delete.
   *
   * @var \Drupal\media_revision_delete\MediaRevisionDeleteInterface
   */
  protected MediaRevisionDeleteInterface $mediaRevisionDelete;

  /**
   * Constructor.
   *
   * @param \Drupal\media_revision_delete\MediaRevisionDeleteInterface $media_revision_delete
   *   The media revision delete.
   */
  public function __construct(MediaRevisionDeleteInterface $media_revision_delete)
  {
    $this->mediaRevisionDelete = $media_revision_delete;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('media_revision_delete')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'media_revision_delete_media_type_revisions_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MediaTypeInterface $media_type = NULL): array
  {
    $this->mediaType = $media_type;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup
  {
    return $this->t('Are you sure you want to delete the candidates revisions for the "%media_type" media type?', ['%media_type' => $this->mediaType->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup
  {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string
  {
    $description = '<p>' . $this->t('This action will delete the candidate revisions for the "@media_type" media type.', ['@media_type' => $this->mediaType->label()]) . '</p>';
    $description .= '<p>' . parent::getDescription() . '</p>';
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText(): TranslatableMarkup
  {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url
  {
    return new Url('media_revision_delete.admin_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    // Getting the media type candidate revisions.
    $candidate_revisions = $this->mediaRevisionDelete->getCandidatesRevisions($this->mediaType->id());

    // Add the batch.
    batch_set($this->mediaRevisionDelete->getRevisionDeletionBatch($candidate_revisions, FALSE));

    // Redirecting.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
