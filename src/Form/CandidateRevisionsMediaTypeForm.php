<?php

namespace Drupal\media_revision_delete\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\media\MediaTypeInterface;
use Drupal\media_revision_delete\MediaRevisionDeleteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Candidate Revisions Media Type Form.
 *
 * @package Drupal\media_revision_delete\Form
 */
class CandidateRevisionsMediaTypeForm extends FormBase
{

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The media revision delete interface.
   *
   * @var \Drupal\media_revision_delete\MediaRevisionDeleteInterface
   */
  protected MediaRevisionDeleteInterface $mediaRevisionDelete;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\media_revision_delete\MediaRevisionDeleteInterface $media_revision_delete
   *   The media revision delete.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MediaRevisionDeleteInterface $media_revision_delete,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mediaRevisionDelete = $media_revision_delete;
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('media_revision_delete'),
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'media_revision_delete_candidates_revisions_media_type';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?MediaTypeInterface $media_type = NULL): array
  {
    // Table header.
    $header = [
      $this->t('Revision ID'),
      [
        'data' => $this->t('Revision'),
        // Hide the description on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      $this->t('mid'),
      [
        'data' => $this->t('Title'),
        // Hide the description on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Operations'),
        // Hide the Operations on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
    ];

    // Getting the media type machine name.
    $media_type_machine_name = $media_type->id();
    // Getting the media revisions.
    $revisions = $this->mediaRevisionDelete->getCandidatesRevisions($media_type_machine_name);

    $rows = [];
    foreach ($revisions as $revision) {
      // Loading the revisions.
      /** @var \Drupal\Core\Entity\RevisionLogInterface $revision */
      $revision = $this->entityTypeManager->getStorage('media')->loadRevision($revision);

      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Build link to view revision.
      $date = $this->dateFormatter->format($revision->revision_created->value, 'short');
      $revision_url = new Url('entity.media.revision', [
        'media' => $revision->id(),
        'media_revision' => $revision->getRevisionId(),
      ]);
      $revision_link = Link::fromTextAndUrl($date, $revision_url)->toRenderable();

      $revision_info = [
        '#type' => 'inline_template',
        '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
        '#context' => [
          'date' => $this->renderer->renderPlain($revision_link),
          'username' => $this->renderer->renderPlain($username),
          'message' => [
            '#markup' => $revision->revision_log->value,
            '#allowed_tags' => Xss::getHtmlTagList(),
          ],
        ],
      ];

      // Getting the mid.
      $mid = $revision->id();
      // Getting the vid.
      $vid = $revision->getRevisionId();
      // The route parameters.
      $route_parameters_destination = [
        'media_type' => $media_type_machine_name,
      ];

      // Return to the same page after save the media type.
      $destination = Url::fromRoute('media_revision_delete.candidate_revisions_media_type', $route_parameters_destination)->toString();
      $destination_options = [
        'query' => ['destination' => $destination],
      ];

      // The route parameters.
      $route_parameters_dropbutton = [
        'media' => $mid,
        'media_revision' => $vid,
      ];

      $dropbutton = [
        '#type' => 'dropbutton',
        '#links' => [
          // Action to delete revisions.
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.media.revision_delete_confirm', $route_parameters_dropbutton, $destination_options),
          ],
        ],
      ];

      $rows[$vid] = [
        $vid,
        ['data' => $revision_info],
        $mid,
        Link::fromTextAndUrl($revision->label(), $revision->toUrl('canonical')),
        ['data' => $dropbutton],
      ];
    }

    $media_type_url = $media_type->toUrl()->toString();
    $caption = $this->t('Candidates revisions for media type <a href=":url">%title</a>', [
      ':url' => $media_type_url,
      '%title' => $media_type->label(),
    ]);

    $form['candidate_revisions'] = [
      '#type' => 'tableselect',
      '#caption' => $caption,
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('There are not candidates revisions to be deleted.'),
      '#sticky' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete revisions'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    // Get selected revisions.
    $candidate_revisions = array_filter($form_state->getValue('candidate_revisions'));

    if (count($candidate_revisions)) {
      // Add the batch.
      batch_set($this->mediaRevisionDelete->getRevisionDeletionBatch($candidate_revisions, FALSE));
    }
  }
}
