<?php

namespace Drupal\media_revision_delete\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\media\MediaTypeInterface;
use Drupal\media_revision_delete\MediaRevisionDeleteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Candidate Medias Form.
 *
 * @package Drupal\media_revision_delete\Form
 */
class CandidateMediasForm extends FormBase
{

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The media revision delete interface.
   *
   * @var \Drupal\media_revision_delete\MediaRevisionDeleteInterface
   */
  protected MediaRevisionDeleteInterface $mediaRevisionDelete;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\media_revision_delete\MediaRevisionDeleteInterface $media_revision_delete
   *   The media revision delete.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MediaRevisionDeleteInterface $media_revision_delete,
    DateFormatterInterface $date_formatter
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mediaRevisionDelete = $media_revision_delete;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('media_revision_delete'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'media_revision_delete_candidates_medias';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?MediaTypeInterface $media_type = NULL): array
  {
    // Table header.
    $header = [
      $this->t('Mid'),
      [
        'data' => $this->t('Title'),
        // Hide the description on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Author'),
        // Hide the description on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Status'),
        // Hide the description on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Updated'),
        // Hide the description on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Candidate revisions'),
        // Hide the description on narrow width devices.
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      $this->t('Operations'),
    ];
    // Table rows.
    $rows = [];

    // Getting the media type machine name.
    $media_type_machine_name = $media_type->id();

    // Getting the candidate medias.
    $candidate_medias = $this->mediaRevisionDelete->getCandidatesMedias($media_type_machine_name);
    $medias = $this->entityTypeManager->getStorage('media')->loadMultiple($candidate_medias);

    // Setup array to store candidate revisions keyed by media id.
    $candidate_revisions = [];

    /** @var \Drupal\media\Entity\Media $media */
    foreach ($medias as $media) {
      $mid = $media->id();

      $route_parameters = [
        'media_type' => $media_type_machine_name,
        'media' => $mid,
      ];

      // Get media's candidate revisions for count display and form_state
      // storage.
      $media_candidate_revisions = $this->mediaRevisionDelete->getCandidatesRevisionsByMids([$mid]);
      $candidate_revisions[$mid] = $media_candidate_revisions;
      $media_revision_count = count($media_candidate_revisions);
      // Formatting the numbers.
      $media_revision_count = number_format($media_revision_count, 0, '.', '.');
      // Creating a link to the candidate revisions page.
      $candidate_revisions_link = Link::createFromRoute($media_revision_count, 'media_revision_delete.candidate_revisions_media', $route_parameters);

      $dropbutton = [
        '#type' => 'dropbutton',
        '#links' => [
          // Action to delete revisions.
          'delete_revisions' => [
            'title' => $this->t('Delete revisions'),
            'url' => Url::fromRoute('media_revision_delete.candidate_medias_revisions_delete_confirm', $route_parameters),
          ],
        ],
      ];

      // Setting the row values.
      $rows[$mid] = [
        $mid,
        Link::fromTextAndUrl($media->getName(), $media->toUrl('canonical')),
        $media->getOwner()->getAccountName() ? Link::fromTextAndUrl($media->getOwner()->getAccountName(), $media->getOwner()->toUrl('canonical')) : $this->t('Anonymous (not verified)'),
        $media->isPublished() ? $this->t('Published') : $this->t('Not published'),
        $this->dateFormatter->format($media->getChangedTime(), 'short'),
        $candidate_revisions_link,
        [
          'data' => $dropbutton,
        ],
      ];
    }

    $media_type_url = $media_type->toUrl()->toString();
    $caption = $this->t('Candidates medias for media type <a href=":url">%title</a>', [
      ':url' => $media_type_url,
      '%title' => $media_type->label(),
    ]);

    $form['candidate_medias'] = [
      '#type' => 'tableselect',
      '#caption' => $caption,
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('There are not candidates medias with revisions to be deleted.'),
      '#sticky' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete revisions'),
      '#button_type' => 'primary',
    ];

    // Add all candidate revisions into form_state for use in
    // the submitForm() method.
    $form_state->set('candidate_revisions', $candidate_revisions);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    // Get selected candidate medias.
    $mids = array_filter($form_state->getValue('candidate_medias'));

    if (count($mids)) {
      // Get selected media's candidate revisions from form_state to delete.
      $candidate_revisions = [];
      $candidate_revisions_by_mid = array_intersect_key($form_state->get('candidate_revisions'), $mids);
      foreach ($candidate_revisions_by_mid as $revisions) {
        $candidate_revisions = array_merge($candidate_revisions, $revisions);
      }

      // Add the batch.
      batch_set($this->mediaRevisionDelete->getRevisionDeletionBatch($candidate_revisions, FALSE));
    }
  }
}
