<?php

namespace Drupal\media_revision_delete\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\media\MediaInterface;
use Drupal\media_revision_delete\MediaRevisionDeleteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a candidate media revision deletion confirmation form.
 */
class CandidateMediasRevisionsDeleteForm extends ConfirmFormBase
{

  /**
   * The media type object.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected MediaInterface $media;

  /**
   * The media revision delete interface.
   *
   * @var \Drupal\media_revision_delete\MediaRevisionDeleteInterface
   */
  protected MediaRevisionDeleteInterface $mediaRevisionDelete;

  /**
   * Constructor.
   *
   * @param \Drupal\media_revision_delete\MediaRevisionDeleteInterface $media_revision_delete
   *   The media revision delete.
   */
  public function __construct(MediaRevisionDeleteInterface $media_revision_delete)
  {
    $this->mediaRevisionDelete = $media_revision_delete;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('media_revision_delete')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'media_revision_delete_candidates_medias_revisions_delete';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $media_type
   *   The media type machine name.
   * @param \Drupal\media\MediaInterface|null $media
   *   The media.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $media_type = NULL, ?MediaInterface $media = NULL): array
  {
    $this->media = $media;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup
  {
    return $this->t('Are you sure you want to delete the candidates revisions for the media "%media_name" ?', ['%media_name' => $this->media->getName()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup
  {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string
  {
    $description = '<p>' . $this->t('This action will delete the candidate revisions for the "@media_name" media type.', ['@media_name' => $this->media->getName()]) . '</p>';
    $description .= '<p>' . parent::getDescription() . '</p>';
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText(): TranslatableMarkup
  {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url
  {
    return new Url('media_revision_delete.candidate_medias', ['media_type' => $this->media->bundle()]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    // Getting the media type candidate revisions.
    $candidate_revisions = $this->mediaRevisionDelete->getCandidatesRevisionsByMids([$this->media->id()]);

    // Add the batch.
    batch_set($this->mediaRevisionDelete->getRevisionDeletionBatch($candidate_revisions, FALSE));

    // Redirecting.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
