<?php

namespace Drupal\media_revision_delete\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media_revision_delete\MediaRevisionDeleteInterface;
use Drupal\media_revision_delete\Utility\Time;
use Drush\Commands\DrushCommands;

/**
 * The Media Revision Delete Commands.
 *
 * @package Drupal\media_revision_delete\Commands
 */
class MediaRevisionDeleteCommands extends DrushCommands
{

  /**
   * The ConfigManager service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The MediaRevisionDelete service.
   *
   * @var \Drupal\media_revision_delete\MediaRevisionDeleteInterface
   */
  protected MediaRevisionDeleteInterface $mediaRevisionDelete;

  /**
   * The DateFormatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * MediaRevisionDeleteCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The ConfigManager service.
   * @param \Drupal\media_revision_delete\MediaRevisionDeleteInterface $mediaRevisionDelete
   *   The MediaRevisionDelete service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The DateFormatter service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The State service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The EntityTypeManager service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    MediaRevisionDeleteInterface $mediaRevisionDelete,
    DateFormatterInterface $dateFormatter,
    StateInterface $state,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->configFactory = $configFactory;
    $this->mediaRevisionDelete = $mediaRevisionDelete;
    $this->dateFormatter = $dateFormatter;
    $this->state = $state;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Deletes old media revisions for a given media type.
   *
   * @param string $type
   *   Media type machine name.
   * @param array $options
   *   The options.
   *
   * @option dry_run Test run without deleting revisions but seeing the output.
   *
   * @usage mrd article
   *   Delete article revisions according to set configuration.
   * @usage mrd page --dry_run
   *   Execute the deletion process without delete the revisions, just to see
   *   the output result.
   *
   * @command media-revision-delete
   * @aliases mrd
   */
  public function mediaRevisionDelete(string $type, array $options = ['dry_run' => FALSE]): void
  {
    // Get all the candidate revisions.
    $candidate_revisions = $this->mediaRevisionDelete->getCandidatesRevisions($type);
    // Checking if this is a dry run.
    if ($options['dry_run']) {
      $this->io()->writeln(dt('This is a dry run. No revision will be deleted.'));
    }

    // Start the batch job.
    batch_set($this->mediaRevisionDelete->getRevisionDeletionBatch($candidate_revisions, $options['dry_run']));
    drush_backend_batch_process();
  }

  /**
   * Configures how many revisions delete per cron run.
   *
   * @param int|null $quantity
   *   Revisions quantity to delete per cron run.
   *
   * @usage mrd-delete-cron-run
   *   Show how many revisions the module will delete per cron run.
   * @usage mrd-delete-cron-run 50
   *   Configure the module to delete 50 revisions per cron run.
   *
   * @command mrd:delete-cron-run
   * @aliases mrd-dcr, mrd-delete-cron-run
   */
  public function deleteCronRun(?int $quantity = NULL): void
  {
    // Getting an editable config because we will get and set a value.
    $config = $this->configFactory->getEditable('media_revision_delete.settings');
    // If no argument found?
    if (!is_null($quantity)) {
      // Saving the values in the config.
      $config->set('media_revision_delete_cron', $quantity);
      $config->save();

      $message = dt('<info>The module was configured to delete @revisions revisions per cron run.</info>', ['@revisions' => $quantity]);
      $this->io()->writeln($message);
    } else {
      // Getting the values from the config.
      $revisions = $config->get('media_revision_delete_cron');
      $message = dt('<info>The revisions quantity to delete per cron run is: @revisions.</info>', ['@revisions' => $revisions]);
      $this->io()->writeln($message);
    }
  }

  /**
   * Get the last time that the media revision delete was made.
   *
   * @usage mrd-last-execute
   *   Show the last time that the media revision delete was made.
   *
   * @command mrd:last-execute
   * @aliases mrd-le, mrd-last-execute
   */
  public function lastExecute(): void
  {
    // Getting the value.
    $last_execute = $this->state->get('media_revision_delete.last_execute', 0);
    if (!empty($last_execute)) {
      $last_execute = $this->dateFormatter->format($last_execute);
      $message = dt('<info>The last time when media revision delete was made was: @last_execute.</info>', ['@last_execute' => $last_execute]);
    } else {
      $message = dt('<info>The removal of revisions through the module media revision delete has never been executed on this site.</info>');
    }
    $this->writeln($message);
  }

  /**
   * Configures the frequency with which to delete revisions while cron run.
   *
   * @param string $time
   *   The time value (never, every_hour, every_time, everyday, every_week,
   *   every_10_days, every_15_days, every_month, every_3_months,
   *   every_6_months, every_year or every_2_years)
   *
   * @usage mrd-set-time
   *   Show a list to select the frequency with which to delete revisions while
   *   cron is running.
   * @usage mrd-set-time every_time
   *   Configure the module to delete revisions every time the cron runs.
   *
   * @command mrd:set-time
   * @aliases mrd-st, mrd-set-time
   */
  public function setTime(string $time = ''): void
  {
    // Getting an editable config because we will get and set a value.
    $config = $this->configFactory->getEditable('media_revision_delete.settings');

    // Check for correct argument.
    $options = Time::convertWordToTime();
    $options_keys = array_keys($options);

    if (!in_array($time, $options_keys)) {
      if (!empty($time)) {
        $this->writeln(dt('"@time_value" is not a valid time argument.', ['@time_value' => $time]));
      }
      $choice = $this->io()->choice(dt('Choose the frequency with which to delete revisions while cron is running:'), $this->mediaRevisionDelete->getTimeValues());
      $time = $options[$options_keys[$choice]];
    } else {
      $time = $options[$time];
    }
    // Saving the values in the config.
    $config->set('media_revision_delete_time', $time);
    $config->save();
    // Getting the values from the config.
    $time_value = $this->mediaRevisionDelete->getTimeValues($time);
    $message = dt('<info>The frequency with which to delete revisions while cron is running was set to: @time.</info>', ['@time' => $time_value]);
    $this->writeln($message);
  }

  /**
   * Shows the frequency with which to delete revisions while cron is running.
   *
   * @usage mrd-get-time
   *   Shows the actual frequency with which to delete revisions while cron is
   *   running.
   *
   * @command mrd:get-time
   * @aliases mrd-gt, mrd-get-time
   */
  public function getTime(): void
  {
    // Getting the config.
    $config = $this->configFactory->get('media_revision_delete.settings');
    // Getting the values from the config.
    $time = $config->get('media_revision_delete_time');
    $time = $this->mediaRevisionDelete->getTimeValues($time);

    $message = dt('<info>The frequency with which to delete revisions while cron is running is: @time.</info>', ['@time' => $time]);
    $this->writeln($message);
  }

  /**
   * Configures the time options for the inactivity time.
   *
   * Configures the time options for the inactivity time that the revision must
   * have to be deleted.
   *
   * @param int|null $max_number
   *   The maximum number for inactivity time configuration.
   * @param int|null $time
   *   The time value for inactivity time configuration (days, weeks or months).
   *
   * @usage mrd-when-to-delete-time
   *   Shows the time configuration for the inactivity time.
   * @usage mrd-when-to-delete-time 30 days
   *   Set the maximum inactivity time to 30 days.
   * @usage mrd-when-to-delete-time 6 weeks
   *   Set the maximum inactivity time to 6 weeks.
   *
   * @command mrd:when-to-delete-time
   * @aliases mrd-wtdt, mrd-when-to-delete-time
   */
  public function whenToDeleteTime(?int $max_number = NULL, ?int $time = NULL): void
  {
    // Getting an editable config because we will get and set a value.
    $config = $this->configFactory->getEditable('media_revision_delete.settings');
    // Getting or setting values?
    if (isset($max_number)) {
      // Saving the values in the config.
      $media_revision_delete_when_to_delete_time['max_number'] = $max_number;
      $media_revision_delete_when_to_delete_time['time'] = $time;
      $config->set('media_revision_delete_when_to_delete_time', $media_revision_delete_when_to_delete_time);
      $config->save();

      $time = $this->mediaRevisionDelete->getTimeNumberString($time) == 1 ? $time['singular'] : $time['plural'];
      // We need to update the max_number in the existing media type
      // configuration if the new value is lower than the actual.
      $this->mediaRevisionDelete->updateTimeMaxNumberConfig('when_to_delete', $max_number);

      $message = dt('<info>The maximum inactivity time was set to @max_number @time.</info>', [
        '@max_number' => $max_number,
        '@time' => $time,
      ]);
      $this->writeln($message);
    } else {
      // Getting the values from the config.
      $media_revision_delete_when_to_delete_time = $config->get('media_revision_delete_when_to_delete_time');
      $max_number = $media_revision_delete_when_to_delete_time['max_number'];
      $time = $this->mediaRevisionDelete->getTimeNumberString($time) == 1 ? $time['singular'] : $time['plural'];

      $message = dt('<info>The maximum inactivity time is: @max_number @time.</info>', [
        '@max_number' => $max_number,
        '@time' => $time,
      ]);
      $this->writeln($message);
    }
  }

  /**
   * Configures time options to know the minimum age.
   *
   * Configures time options to know the minimum age. that the revision must
   * have to be delete.
   *
   * @param int|null $max_number
   *   The maximum number for minimum age configuration.
   * @param int|null $time
   *   The time value for minimum age configuration (days, weeks or months).
   *
   * @usage mrd-minimum-age-to-delete-time
   *   Shows the time configuration for the minimum age of revisions.
   * @usage mrd-minimum-age-to-delete-time 30 days
   *   Set the maximum time for the minimum age to 30 days.
   * @usage mrd-minimum-age-to-delete-time 6 weeks
   *   Set the maximum time for the minimum age to 6 weeks.
   *
   * @command mrd:minimum-age-to-delete-time
   * @aliases mrd-matdt, mrd-minimum-age-to-delete-time
   */
  public function minimumAgeToDeleteTime(?int $max_number = NULL, ?int $time = NULL): void
  {
    // Getting an editable config because we will get and set a value.
    $config = $this->configFactory->getEditable('media_revision_delete.settings');
    // Getting or setting values?
    if (isset($max_number)) {
      // Saving the values in the config.
      $media_revision_delete_minimum_age_to_delete_time['max_number'] = $max_number;
      $media_revision_delete_minimum_age_to_delete_time['time'] = $time;
      $config->set('media_revision_delete_minimum_age_to_delete_time', $media_revision_delete_minimum_age_to_delete_time);
      $config->save();

      // We need to update the max_number in the existing media type
      // configuration if the new value is lower than the actual.
      $this->mediaRevisionDelete->updateTimeMaxNumberConfig('minimum_age_to_delete', $max_number);
      $time = $this->mediaRevisionDelete->getTimeNumberString($time) == 1 ? $time['singular'] : $time['plural'];

      // Is singular or plural?
      $message = dt('<info>The maximum time for the minimum age was set to @max_number @time.</info>', [
        '@max_number' => $max_number,
        '@time' => $time,
      ]);
      $this->writeln($message);
    } else {
      // Getting the values from the config.
      $media_revision_delete_minimum_age_to_delete_time = $config->get('media_revision_delete_minimum_age_to_delete_time');
      $max_number = $media_revision_delete_minimum_age_to_delete_time['max_number'];
      $time = $this->mediaRevisionDelete->getTimeNumberString($time) == 1 ? $time['singular'] : $time['plural'];

      // Is singular or plural?
      $message = dt('<info>The maximum time for the minimum age is: @max_number @time.</info>', [
        '@max_number' => $max_number,
        '@time' => $time,
      ]);
      $this->writeln($message);
    }
  }

  /**
   * Delete all revisions prior to a revision.
   *
   * @param int $mid
   *   The id of the media which revisions will be deleted.
   * @param int $vid
   *   The revision id, all prior revisions to this revision will be deleted.
   *
   * @usage mrd-delete-prior-revisions 1 3
   *   Delete all revisions prior to revision id 3 of media id 1.
   * @command mrd:delete-prior-revisions
   * @aliases mrd-dpr,mrd-delete-prior-revisions
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function deletePriorRevisions(int $mid = 0, int $vid = 0): void
  {
    // Get list of prior revisions.
    $previousRevisions = $this->mediaRevisionDelete->getPreviousRevisions($mid, $vid);

    if (count($previousRevisions) === 0) {
      $this->writeln(dt('<error>No prior revision(s) found to delete.</error>'));
      return;
    }

    if ($this->io()->confirm(dt("Confirm deleting @count revision(s)?", ['@count' => count($previousRevisions)]))) {
      // Check if current revision should be deleted, too.
      if ($this->io()->confirm(dt("Additionally, do you want to delete the revision @vid? @count revision(s) will be deleted.", [
        '@vid' => $vid,
        '@count' => count($previousRevisions) + 1,
      ]))) {
        $this->entityTypeManager->getStorage('media')->deleteRevision($vid);
      }

      foreach ($previousRevisions as $revision) {
        $this->entityTypeManager->getStorage('media')->deleteRevision($revision->getRevisionId());
      }
    }
  }

  /**
   * Validate inputs before executing the drush command media-revision-delete.
   *
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *   The command data.
   *
   * @return bool
   *   Returns TRUE if the validations has passed FALSE otherwise.
   *
   * @hook validate mrd
   */
  public function mediaRevisionDeleteValidate(CommandData $commandData): bool
  {
    $input = $commandData->input();
    $type = $input->getArgument('type');

    if (!$this->configuredMediaType($type)) {
      return FALSE;
    }

    // Checking if we have candidates medias to delete.
    $candidates = count($this->mediaRevisionDelete->getCandidatesRevisions($type));
    if (!$candidates) {
      $this->io()->warning(dt('There are no revisions to delete for @media_type.', ['@media_type' => $type]));
    }

    return TRUE;
  }

  /**
   * Validate inputs before executing the drush command mrd-dpr.
   *
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *   The command data.
   *
   * @return bool
   *   Returns TRUE if the validations has passed FALSE otherwise.
   *
   * @hook validate mrd-delete-prior-revisions
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function deletePriorRevisionsValidate(CommandData $commandData): bool
  {
    $input = $commandData->input();
    $mid = $input->getArgument('mid');
    $vid = $input->getArgument('vid');

    // Check if argument mid is a valid media id.
    $media_storage = $this->entityTypeManager->getStorage('media');
    $media = $media_storage->load($mid);
    if (is_null($media)) {
      $this->io()->error(dt("@mid is not a valid media id.", ['@mid' => $mid]));
      return FALSE;
    }

    // Get all revisions of the current media, in all languages.
    $revision_ids = $media_storage->revisionIds($media);
    if (!in_array($vid, $revision_ids)) {
      $variables = [
        '@vid' => $vid,
        '@mid' => $mid,
      ];
      $this->io()->error(dt("@vid is not a valid revision id for media @mid.", $variables));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Untrack a media type.
   *
   * @param string $type
   *   The media type name.
   *
   * @usage mrd-untrack article
   *   Untrack the media type article.
   * @command mrd:untrack
   * @aliases mrd-u, mrd-untrack
   */
  public function untrack(string $type): void
  {
    $this->mediaRevisionDelete->deleteMediaTypeConfig($type);
    $message = dt('<info>The media type @type is now untracked.</info>', ['@type' => $type]);
    $this->writeln($message);
  }

  /**
   * Validate inputs before executing the drush command mrd-untrack.
   *
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *   The command data.
   *
   * @return bool
   *   Returns TRUE if the validations has passed FALSE otherwise.
   *
   * @hook validate mrd-u
   */
  public function untrackValidate(CommandData $commandData): bool
  {
    $input = $commandData->input();
    $type = $input->getArgument('type');

    if (!$this->configuredMediaType($type)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Validate inputs before executing the drush command mrd-untrack.
   *
   * @param string $type
   *   The media type name.
   *
   * @return bool
   *   Returns if a media type is configured.
   */
  private function configuredMediaType(string $type): bool
  {
    // Make sure the media type exists and is configured.
    $available_media_types = array_map(static function ($media_type) {
      /** @var \Drupal\media\MediaTypeInterface $media_type */
      return $media_type->id();
    }, $this->mediaRevisionDelete->getConfiguredMediaTypes());

    if (!in_array($type, $available_media_types, TRUE)) {
      $this->io()->error(dt(
        'The following media type is not configured for revision deletion: @name',
        [
          '@name' => $type,
        ]
      ));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Track a media type into media revision delete system.
   *
   * @usage mrd-track article 50 1 15
   *   Track the article media type with:
   *   50 minimum number of revision to keep.
   *   1 minimum age of revision to delete.
   *   15 when to delete the revisions.
   *
   * @command mrd:track
   * @aliases mrd-t, mrd-track
   */
  public function track(string $media_type, int $minimum_revisions_to_keep, int $minimum_age_to_delete, int $when_to_delete): bool
  {
    // Validate for a valid media type.
    $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
    $media_types_ids = array_map(function (MediaType $object) {
      return $object->id();
    }, $media_types);

    if (!in_array($media_type, $media_types_ids)) {
      $this->io()->error(dt('Argument media type is not a valid media type.'));
      return FALSE;
    }

    // Validate for Maximum number allowed.
    $config = $this->configFactory->getEditable('media_revision_delete.settings');
    $media_revision_delete_minimum_age_to_delete_time = $config->get('media_revision_delete_minimum_age_to_delete_time');
    $media_revision_delete_when_to_delete_time = $config->get('media_revision_delete_when_to_delete_time');
    if ($minimum_age_to_delete > $media_revision_delete_minimum_age_to_delete_time['max_number']) {
      $this->io()->error(dt('Argument minimum_age_to_delete must lower or equal to @number', ['@number' => $media_revision_delete_minimum_age_to_delete_time['max_number']]));
      return FALSE;
    }

    if ($when_to_delete > $media_revision_delete_when_to_delete_time['max_number']) {
      $this->io()->error(dt('Argument when_to_delete must lower or equal to @number.', ['@number' => $media_revision_delete_when_to_delete_time['max_number']]));
      return FALSE;
    }

    $this->mediaRevisionDelete->saveMediaTypeConfig($media_type, $minimum_revisions_to_keep, $minimum_age_to_delete, $when_to_delete);
    $message = dt('<info>The media type @media_type is now tracked.</info>', ['@media_type' => $media_type]);
    $this->writeln($message);

    return TRUE;
  }
}
